const SCHEMA_ERRORS = {
    PATTERN: 'pattern disagreement',
    NOT_FOUND: 'element not found at schema',
    REQUIRED: 'element required',
    MAX_LENGTH: 'max length surpassed',
    MIN_LENGTH: 'min length not reached',
    TYPE: 'type mismatch'
};

module.exports = { SCHEMA_ERRORS };