/**
 * TESTS FOR ADDRESS SCHEMA
 */
'use strict';

const check = require('../index');
const { should, expect } = require('chai');
const { SCHEMA_ERRORS } = require('../constants');
const { addressSchema } = require('./schemas');

const obj = {
  name: 'Rogério de Oliveira Batista',
  address: {
    country: 'Brasil',
    neighborhood: 'Centro',
    state: { stateName: 'Minas Gerais', stateCode: 31 },
    city: {
      cityName: 'Pouso Alegre',
      zipCode: '31110-000',
      neighborhood: 'Centro',
      street: 'Rua dos Tamóios, 45'
    }
  },
  phone: '(38)99809-8256'
};

describe('Address schema validation', () => {
  it('checks a valid address', done => {
    check(obj, addressSchema, err => {
      expect(err).to.be.empty;
      done();
    });
  });
  it('checks the following invalid props: zipCode', done => {
    addressSchema.address.city.zipCode.maxLength = 5;
    check(obj, addressSchema, err => {
      expect(err).to.not.be.empty;
      expect(err[0].message).to.be.equal(SCHEMA_ERRORS.MAX_LENGTH);
      done();
    }, true);
  });
});
