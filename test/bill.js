
/**
 * TESTS FOR BILL SCHEMA
 */
'use strict';

const check = require('../index');
const { should, expect } = require('chai');
const { SCHEMA_ERRORS } = require('../constants');
const { billSchema } = require('./schemas');

const billOne = {
    productDescription: 'Frozen Vegetables',
    value: 12.55,
    date: '2023-08-02'
};
const billTwo = {
    productDescription: 'n/a',
    value: 0,
    date: '2019-02-022222'
};

describe('Bill schema validation', () => {
    it('should a valid bill', done => {
        check(billOne, billSchema, err => {
            expect(err).to.be.empty;
            done();
        })
    });
    it('checks the following invalid props: date and minLength', done => {
        check(billTwo, billSchema, err => {
            expect(err[0].message).to.be.equal(SCHEMA_ERRORS.MIN_LENGTH)
            expect(err[1].message).to.be.equal(SCHEMA_ERRORS.PATTERN)
            done();
        })
    });
});