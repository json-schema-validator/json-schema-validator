const addressSchema = {
    name: { type: 'string', maxLength: 30 },
    address: {
        country: {
            type: 'string',
            maxLength: 100,
            minLength: 3,
            required: true
        },
        state: {
            stateName: { type: 'string', maxLength: 15 },
            stateCode: { type: 'number', maxLength: 2 }
        },
        city: {
            cityName: { type: 'string', maxLength: 60 },
            zipCode: { type: 'string', maxLength: 10 },
            street: { type: 'string', maxLength: 25 },
            neighborhood: { type: 'string', maxLength: 10 }
        }
    },
    phone: { type: 'string', maxLength: 15, pattern: '^\\(\\d{2}\\)\\d{4,5}-\\d{4}$' }
};

const billSchema = {
    productDescription: {
        type: 'string',
        maxLength: 200,
        minLength: 10,
        required: true
    },
    value: {
        type: 'number',
        required: true,
        min: 0.1,
        max: 10e9
    },
    date: {
        type: 'string',
        required: true,
        pattern: '^\\d{4}-\\d{2}-\\d{2}$'
    }
};

module.exports = { addressSchema, billSchema };