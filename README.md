# JSON Schema Validator

<img width="200" src="./thumbnail.png" alt="JSON schema validator" style="align: center">

A lightweight and straightforward algorithm that validates a JSON document against a given schema. The code is built without any dependencies and can be replicated easily in any JavaScript app.

This type of code is pretty handy for various scenarios, such as communication between processes, APIs, printers, applications, and so on. The JSON format is widely used to transport data, and depending on its purpose, it is necessary to establish some contracts regarding data format. Such a principle is important to ensure that the recipient receives the appropriate content.

## Computational cost

A JSON document has the same structure as a tree. Each node has a hierarchical tree structure. Thus, iterating through its keys takes linear time O(n). Complexity is related to its number of levels.

For instance, the following JSON:

```
{ 
  node1: 'value', 
  node2: { 
    node3: { 
      node5: 'value'
    }, 
    node4: 'value'
  }
}
```
represents the following tree:

                    root
                  /      \
              node1     node2
                        /   \
                    node3   node4
                    ...

### How does it work?
The easiest way to iterate over the whole JSON document is to start at the top (root) and go down through all the child nodes by passing each reached node (root) into a recursive method. The ``check()`` method uses this technique to travel across nodes. 

For each node in the schema, we check if the current object subtree contains the corresponding key at the same level.

For instance, taking the next schema and object respectively will produce an invalid output.

```json
{
  "userName": {
    "type": "string",
    "required": true,
    "min": 10,
    "max": 150
  },
  "address": {
    "country": {
      "type": "string",
      "required": true,
      "min": 5,
      "max": 100
    },
    "state": {
      "type": "string",
      "required": false,
      "min": 5,
      "max": 100
    }
  }
}
```

```javascript
const obj = {
  userName: 'Anya Taylor-Joy',
  country: 'Argentina',
  address: {
    state: 'Córdoba'
  }
}
```

The error related to the above example occurs because the `country` key does not respect the hierarchy established by the schema used to validate its content.

A successful validation is achieved when the `error` parameter, present in the callback function, is empty (more information on this can be found in the usage examples section). For more details on how this code functions, please refer to the testing folder.


## Project setup

### Installation of dev dependencies

```bash
npm install
```

### Running tests

```bash
npm run test
```

## Schema structure
The schemas must be defined according to the JSON format. For each schema field, you can define its validation rules using the following options:


|  Key 	| Type  	|  Description 	|
|---	|---	|---	|
| required  	|  Boolean 	|  If its value is `true`, that means the current key is required. For the non-required kyes (optional), if there are other validation rules, they will be performed. 	|
|  pattern 	|  String (Regex) 	|  A valid regex used to evaluate the target value. 	|
|   type	|   String	|  One of the following data types: `string, number, bigint, boolean, object`|
|  min 	|  Number 	|  The min length for text contents and the min value for numbers. 	|
|  max 	|  Number 	|  The max length for text contents and the max value for numbers. 	|


**For each key defined within the schema, the algorithm considers it a prerequisite. During the evaluation process, if a key is not found, the current object is marked as invalid.**

The following example shows a valid schema:

```json
{
    "userId": {
        "type": "number",
        "required": true,
        "min": 1,
        "max": 1000
    },
    "userName": {
        "type": "string",
        "required": true,
        "min": 10,
        "max": 150
    },
    "userEmail": {
        "type": "string",
        "required": false,
        "pattern": "^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$/"
    },
    "address": {
        "street": {
            "type": "string",
            "max": 200
        },
        "number": {
            "type": "number",
            "required": false,
            "min": 0
        },
        "cityName": {
            "type": "string",
            "required": true,
            "min": 5
        },
        "zipCode": {
            "type": "string",
            "required": true,
            "min": 10,
            "max": 10
        }
    }
}
```


## Usage examples

To use the **JSON Schema Validator** in your project, it is necessary to import the `check` method located in "index.js". Besides, the check method requires two parameters: the target object and the target schema, respectively.

```javascript
// Usage example
const jsonCheck = require('index.js');
const schema = require('schemas/address.js');

const data = {
  cityName: 'City Name',
  zipCode: '35780-000'
};

jsonCheck(data, schema, err => {
  // err should be empty for successfull validations.
  if (err.length > 0) {
    console.log('Something went wrong...', err);
  }
});
```

## Improvements

- Stop recursion when the first inconsistency is found in the object.
  It may be made with a flag like `proceed`, if it is true, analysts process will continue, on the other hand, it will stop when the first error is reached. However, to do it, we must replace `forEach` by `for` because forEach does not allow returns inside its structure.

- Validation for arrays: min and max items.

- Built-in validators (ipv4, ipv6, uuid, email, uri, date, etc).

